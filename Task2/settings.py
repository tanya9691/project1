"""
Django settings for Task2 project.

Generated by 'django-admin startproject' using Django 3.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from pathlib import Path
import os
from sshtunnel import SSHTunnelForwarder




# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '2fd-fc0%)k!da#q8!k74&k)*u&@2@0l^q5t#38mewkm0py62$b'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [

    'rest_framework',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'Task2.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Task2.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases
############################################################
# from sshtunnel import SSHTunnelForwarder
# import pandas as pd
# import pymongo
# # import dnspython
# from pandas.io.json import json_normalize
#
# current_path = os.path.dirname(__file__)
# # set the variables
# host = '3.6.163.192'
# mongodb_private_ip = '10.0.4.94'
# ssh_username = 'ubuntu'
# # ssh_private_key = 'chatbot_bastion.pem' # keep the pem file in same directory
# ssh_private_key =current_path +'chatbot_bastion.pem' # keep the pem file in same directory
#
# #Creating a SSH tunnel for local port
# server = SSHTunnelForwarder(
#            (host, 22),
#                ssh_username=ssh_username ,
#                    ssh_private_key=ssh_private_key,
#                        remote_bind_address=(mongodb_private_ip, 27017),
#                            local_bind_address=("127.0.0.1",8001)
#                           )
# server.start()
#
# # username and password for database
# user = "developer"
# passwd = "chatbotXdevilpassw0rd"
# host = "localhost"
# port = server.local_bind_port
# database = "chatbot-database"
#
# #uri of database
# uri = "mongodb://{0}:{1}@{2}:{3}/{4}".format(user,passwd,host,port,database)
#
# client = pymongo.MongoClient(uri)
# print('Client Created')
#
# db = client['chatbot-database']
#
# #############################################
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': BASE_DIR / 'db.sqlite3',
#     }
# }
current_path = os.path.dirname(__file__)

from sshtunnel import SSHTunnelForwarder

host = '3.6.163.192'
mongodb_private_ip = '10.0.4.94'
ssh_username = 'ubuntu'
ssh_private_key = current_path + '/chatbot_bastion.pem'

server = SSHTunnelForwarder((host, 22),
                            ssh_username=ssh_username,
                            ssh_private_key=ssh_private_key,
                            remote_bind_address=(mongodb_private_ip, 27017),
                            local_bind_address=('127.0.0.1', 27017))
server.start()

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

user = 'developer'
passwd = 'chatbotXdevilpassw0rd'
host = 'localhost'
port = server.local_bind_port
database = 'chatbot-database'

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
