import pymongo
import os
import json
import requests
import pandas as pd
from pandas.io.json import json_normalize
from rest_framework.decorators import api_view
from sshtunnel import SSHTunnelForwarder

current_path = os.path.dirname(__file__)
host = '3.6.163.192'
mongodb_private_ip = '10.0.4.94'

ssh_username = 'ubuntu'
ssh_private_key = current_path + '/chatbot_bastion.pem'

print("sever ready to start")

server = SSHTunnelForwarder((host, 22), ssh_username=ssh_username, ssh_private_key=ssh_private_key, remote_bind_address=
(mongodb_private_ip, 27017), local_bind_address=("127.0.0.1", 8000))

server.start()

print("sever started and running at port ", server.local_bind_port)

user = "developer"
passwd = "chatbotXdevilpassw0rd"
host = "localhost"
port = server.local_bind_port
database = "chatbot-database"

uri = "mongodb://{0}:{1}@{2}:{3}/{4}".format(user, passwd, host, port, database)
# uri = "mongodb://{0}:{1}@{2}:{3}/{4}".format(user, passwd, mongodb_private_ip, port, database)

print(uri)
client = pymongo.MongoClient(uri)

print('Client Created')
db = client['chatbot-database']
col1 = db['DemandResponses']
#Prints all the documents in the collection DemandResponses
# for x in col1.find():
#     print(x)
col2=db['NewCollection'] #Creating a New collection in Sapio Database
somevalue = {'id': 1, 'name': 'Jessa'}
col2.insert_one(somevalue)
for y in col2.find():
    print(y)

value=None
# @api_view(['POST'])
'''-------------------------------------------------------------------------------------------------------------'''
def fetch_data(requests,value):
    data=json.loads(requests.body)
    print(data)
    return requests.Response("Got that")

